/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallerinterfaz24;

/**
 *
 * @author Francisco
 */
public class Ventana extends javax.swing.JFrame {

    /**
     * Creates new form Ventana
     */
    public Ventana() {
        initComponents();
        setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        Opciones = new javax.swing.JMenu();
        CheckBox = new javax.swing.JMenuItem();
        Colores = new javax.swing.JMenuItem();
        Lista1 = new javax.swing.JMenuItem();
        arbol = new javax.swing.JMenuItem();
        Tabla1 = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        practica = new javax.swing.JMenuItem();
        btnSalir = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Opciones.setText("Opciones");

        CheckBox.setText("CheckBox");
        CheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxActionPerformed(evt);
            }
        });
        Opciones.add(CheckBox);

        Colores.setText("Colores");
        Colores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ColoresActionPerformed(evt);
            }
        });
        Opciones.add(Colores);

        Lista1.setText("Lista");
        Lista1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Lista1ActionPerformed(evt);
            }
        });
        Opciones.add(Lista1);

        arbol.setText("Arbol");
        arbol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                arbolActionPerformed(evt);
            }
        });
        Opciones.add(arbol);

        Tabla1.setText("Tabla1");
        Tabla1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Tabla1ActionPerformed(evt);
            }
        });
        Opciones.add(Tabla1);

        jMenuItem1.setText("BarraProgreso");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        Opciones.add(jMenuItem1);

        practica.setText("Practica");
        practica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                practicaActionPerformed(evt);
            }
        });
        Opciones.add(practica);

        jMenuBar1.add(Opciones);

        btnSalir.setText("Salir");
        btnSalir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSalirMouseClicked(evt);
            }
        });
        jMenuBar1.add(btnSalir);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 278, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSalirMouseClicked
        System.exit(0);
    }//GEN-LAST:event_btnSalirMouseClicked

    private void ColoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ColoresActionPerformed
        ColoresRadioButton ventanaterciario=new ColoresRadioButton(this,true);
        ventanaterciario.pack();
        ventanaterciario.setVisible(true);
    }//GEN-LAST:event_ColoresActionPerformed

    private void CheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxActionPerformed
       CheckBox ventanasecundaria=new CheckBox(this,true);
       ventanasecundaria.pack();
       ventanasecundaria.setVisible(true);
    }//GEN-LAST:event_CheckBoxActionPerformed

    private void Lista1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Lista1ActionPerformed
       Lista ventanacuartaria=new Lista(this, true);
       ventanacuartaria.pack();
       ventanacuartaria.setVisible(true);
       
    }//GEN-LAST:event_Lista1ActionPerformed

    private void arbolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_arbolActionPerformed
        Arbol1 ventanaquintaria=new Arbol1(this, true);
        ventanaquintaria.pack();
        ventanaquintaria.setVisible(true);
    }//GEN-LAST:event_arbolActionPerformed

    private void Tabla1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Tabla1ActionPerformed
         tabla ventanasextaria=new tabla(this, true);
        ventanasextaria.pack();
        ventanasextaria.setVisible(true);
    }//GEN-LAST:event_Tabla1ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
       barraprogreso ventanaseptaria=new barraprogreso(this, true);
        ventanaseptaria.pack();
        ventanaseptaria.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void practicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_practicaActionPerformed
       practica1 ventanaoptaria=new practica1(this, true);
        ventanaoptaria.pack();
        ventanaoptaria.setVisible(true);
    }//GEN-LAST:event_practicaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem CheckBox;
    private javax.swing.JMenuItem Colores;
    private javax.swing.JMenuItem Lista1;
    private javax.swing.JMenu Opciones;
    private javax.swing.JMenuItem Tabla1;
    private javax.swing.JMenuItem arbol;
    private javax.swing.JMenu btnSalir;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem practica;
    // End of variables declaration//GEN-END:variables
}
